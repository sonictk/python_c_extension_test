import setuptools
from distutils.core import setup, Extension

setup(name="multiply", version="1.0.0", ext_modules = [Extension("multiply", ["multiply.c"])])
