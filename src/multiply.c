#include "Python.h"

double multiply_test(double x, double y)
{
	return x * y;
}

PyDoc_STRVAR(multiply__doc__, "test multiplication module function");
PyDoc_STRVAR(multiply_test__doc__, "test multiplication function");

static PyObject *py_multiply_test(PyObject* self, PyObject *args)
{
	double x = 0;
	double y = 0;
	double result = 0.0;
	if (!PyArg_ParseTuple(args, "dd", &x, &y))
	{
		return NULL;
	}

	result = multiply_test(x, y);

	return Py_BuildValue("d", result);
}

static PyMethodDef multiply_methods[] = {
	{"multiply_test_python", py_multiply_test, METH_VARARGS, multiply_test__doc__},
	{NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC initmultiply(void)
{
	Py_InitModule3("multiply", multiply_methods, multiply__doc__);
}
