# About

This is an example of a Python C extension.

# Installation

Run ``python setup.py build`` in the ``src`` subdirectory in order to build the
extension. The built extension will be in a ``build`` subfolder.
